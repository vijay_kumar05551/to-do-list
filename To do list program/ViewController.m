//
//  ViewController.m
//  To do list program
//
//  Created by Click Labs135 on 9/30/15.
//  Copyright (c) 2015 vijay kumar. All rights reserved.
//

#import "ViewController.h"
NSMutableArray *tableData;
@interface ViewController ()

@property (strong, nonatomic) IBOutlet UILabel *label;
@property (strong, nonatomic) IBOutlet UIButton *goButton;
@property (strong, nonatomic) IBOutlet UITextField *enterItems;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UITableView *showData;

@end

@implementation ViewController
@synthesize label;
@synthesize goButton;
@synthesize enterItems;
@synthesize submitButton;
@synthesize  showData;



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    enterItems.hidden=YES;
    submitButton.hidden=YES;
    tableData=[[NSMutableArray alloc]init];
    [goButton addTarget:self action:@selector(goButtonPressed :) forControlEvents:UIControlEventTouchUpInside];
    
    [submitButton addTarget:self action:@selector(submitButtonPressed :) forControlEvents:UIControlEventTouchUpInside];
    
}
-(void)goButtonPressed:(UIButton *)sender
{
    enterItems.hidden=NO;
    submitButton.hidden=NO;
}
-(void)submitButtonPressed:(UIButton *)sender
{
    [tableData addObject:enterItems.text];
    [showData reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView

{
    
    // Return the number of sections.
    
    return 1;
    
}





- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section

{
    
    return [tableData count];
    
}



-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"cell1"];
    
    cell.textLabel.text=tableData[indexPath.row];
    
    cell.imageView.image=[UIImage imageNamed:@"/Users/clicklabs135/Desktop/To do list program/To do list program/black-iscreen-computer_318-9552.jpg"];
    
    return cell;
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
